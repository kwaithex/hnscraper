# Scraper
#### Run it with docker 
Open `docker-compose.yml` and fill out `app` and `db` service environment variables. You will only need to change `DB_DATABASE` and `DB_PASSWORD` variables for `app` then set the same values in `db` service environment variables `MYSQL_DATABASE` and `MYSQL_ROOT_PASSWORD` **(Database will be created automatically)**

Run docker-compose command that will pull mysql,nginx images and build image for app itself, using php7.4-fpm:

    docker-compose up -d

Run config and cache clearance commands:

    docker-compose exec app php artisan cache:clear
    docker-compose exec app php artisan config:cache

Run database migration command:

    docker-compose exec app php artisan migrate

To start scraper process, run:

    docker-compose exec app php artisan scraper:run

App should be accessible at: http://localhost

#### Run it without docker:
- You need `mysql` (5.7) and create database beforehand
- You need `composer` (v2 recommended)
- You need `npm` (v6.4 recommended)

Copy `.env.example` and name it `.env` and fill all the information for database:

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=dbname
    DB_USERNAME=dbuser
    DB_PASSWORD=dbpass

Scraping target url, records per page is already set in `.env.example` file:

    TARGET_URL=https://news.ycombinator.com/news
    TARGET_BASE_URL=https://news.ycombinator.com/
    RECORDS_PER_PAGE=10

Install all needed PHP and JS dependencies:

    composer install
    npm install

Generate laravel encryption key:

    php artisan key:generate

Run config and cache clearance commands:

    php artisan cache:clear
    php artisan config:cache

Run database migration command:

    php artisan migrate

Compile frontend:

    npm run prod

Run development server startup:

    php artisan serve

To start scraping process, run:

    php artisan scraper:run

App should be accessible at: http://localhost:8000
