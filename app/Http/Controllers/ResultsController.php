<?php

namespace App\Http\Controllers;

use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * @var PostService
     */
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Returns paginated posts by passed filters in json
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function results(Request $request): JsonResponse
    {
        $data = $this->postService->getAllPostsByFilter($request);

        return response()->json([
            'data' => $data->getCollection(),
            'count' => $data->total()
        ]);
    }

    /**
     * Returns scraper config values in json for frontend
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function scraperConfig(Request $request): JsonResponse
    {
        return response()->json([
            config('services.scraper')
        ]);
    }

}
