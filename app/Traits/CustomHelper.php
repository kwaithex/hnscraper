<?php

namespace App\Traits;

/**
 * Helper functions
 */
trait CustomHelper {

    /**
     * Splits string by whitespace, converts result's
     * first half to integer
     *
     * @param string $string
     * @return int
     */
    public static function explodeStringByWhitespaceAndToInt(string $string): int
    {
        return (int) explode(" ", $string)[0];
    }

    /**
     * Random user agent picker to minimize suspicion :)
     *
     * @return string
     */
    public static function randomUserAgent(): string
    {
        $randomUserAgents = [
            'Mozilla/5.0 (Linux; Android 6.0; EVA-L19) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.50 Mobile Safari/537.36',
            'Mozilla/5.0 (Linux; Android 7.1.2; Redmi 4X Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/94.0.4606.85 Mobile Safari/537.36 [FB_IAB/Orca-Android;FBAV/336.0.0.13.142;]',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; .NET CLR 2.1.42118)',
            'Mozilla/5.0 (Linux; Android 10; Infinix X656) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.50 Mobile Safari/537.36',
            'Mozilla/5.0 (Linux; Android 8.1.0; 9009G Build/O11019) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.85 Safari/537.36',
            'Mozilla/5.0 (Linux; Android 6.0.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Mobile Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/11.0b9935 Mobile/16D57 Safari/605.1.15',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 Instagram 86.0.0.11.85 (iPhone11,2; iOS 12_1_4; ru_GE; ru-GE; scale=3.00; gamut=wide; 1125x2436; 146857293)',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 Instagram 89.0.0.14.100 (iPhone10,4; iOS 12_1_4; ru_RU; ru; scale=2.00; gamut=normal; 750x1334; 149781277)',
            'Mozilla/5.0 (Linux; Android 11; SM-A505G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.62 Safari/537.36',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4660.4 Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 12_5_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 OPT/3.1.6'
        ];

        return $randomUserAgents[array_rand($randomUserAgents, 1)];
    }
}
