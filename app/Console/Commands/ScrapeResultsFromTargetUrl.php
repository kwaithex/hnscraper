<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ScraperService;
use App\Jobs\GatherPost;
use Illuminate\Support\Facades\Artisan;

class ScrapeResultsFromTargetUrl extends Command
{
    /**
     *
     * @var ScraperService
     */
    protected $scraperService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gather list of IDs from external API and put them in queue for data gathering';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ScraperService $scraperService)
    {
        parent::__construct();
        $this->scraperService = $scraperService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue? Scraping takes some time', true)) {
            $this->line('Starting to scrape url..');
            $results = $this->scraperService->crawl();

            if ($results) {
                $this->line("Scraped ". count($results) ." pages, will add them to queue for further processing");

                foreach ($results as $page_results) {
                    foreach ($page_results as $post) {
                        GatherPost::dispatch($post);
                    }
                }

                $this->info("Scraping successfully completed, all posts added to queue for db insertion");

                if ($this->confirm('Do you want to start queue processing?', true)) {
                    Artisan::call('queue:work --stop-when-empty', []);
                    $this->info("All queues processed, exiting..");
                } else {
                    $this->error("Didn't confirm, exiting..");
                }

            } else {
                $this->error("GET request returned null response (check logs)");
            }

        } else {
            $this->error("Didn't confirm, exiting..");
        }
    }
}
