<?php

namespace App\Services;

use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class PostService
{

    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Gets all the posts by passed filters
     *
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function getAllPostsByFilter(Request $request): LengthAwarePaginator
    {
        $query = $request->get('query') ?? null;
        $orderDirection = $request->get('ascending') ? 'asc' : 'desc';
        $orderBy = $request->get('orderBy') ?? 'id';

        return $this->postRepository->getAllByFilter($query, $orderBy, $orderDirection);
    }

    /**
     * Creates post
     *
     * @param array $data
     * @return Post
     */
    private function createPost(array $data): Post
    {
        return $this->postRepository->insert($data);
    }

    /**
     * Creates entirely new post or updates post's score
     * based on incoming id (external_id) gathered from scraping
     *
     * @param array $data
     * @return bool
     */
    public function createOrUpdatePost(array $data): bool
    {
        if (isset($data['id'])) {
            if ($existing_post = $this->postExistsByExternalId($data['id'])) {
                if ($existing_post->score != $data['score']) {
                    Log::info("[EXTERNAL ID FOUND: ".$data['id']."] Will update");

                    return $existing_post->update([
                        'score' => $data['score']
                    ]);
                }
            } else {
                Log::info("Creating new post in database (external id: ".$data['id']." )");

                $this->createPost([
                    'external_id' => $data['id'],
                    'title' => $data['title'],
                    'url' => $data['url'],
                    'external_created_at' => $data['date'],
                    'score' => $data['score']
                ]);

                return true;
            }
        }

        return false;
    }

    /**
     * Checks by external_id whether post exists in the database
     *
     * @param int $externalId
     * @return Post|null
     */
    private function postExistsByExternalId(int $externalId): ?Post
    {
        return $this->postRepository->getByExternalId($externalId) ?? null;
    }
}
