<?php

namespace App\Services;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Traits\CustomHelper;

class ScraperService
{
    use CustomHelper;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $results;

    /**
     * @var int
     */
    private $pageInProgress;

    /**
     * First DOM element to go into
     */
    private const DATA_START_SELECTOR = 'table.itemlist';
    /**
     * Main data DOM element to go into
     */
    private const MAIN_DATA_DOM_SELECTOR = 'tr.athing';
    /**
     * Additional data DOM element to go into
     */
    private const ADDITIONAL_DATA_DOM_SELECTOR = 'td.subtext';
    /**
     * DOM element that holds post's url value
     */
    private const URL_SELECTOR = 'a.titlelink';
    /**
     * DOM element that holds post's title value
     */
    private const TITLE_SELECTOR = 'a.titlelink';
    /**
     * DOM element that holds post's date created value
     */
    private const DATE_SELECTOR = 'span.age';
    /**
     * DOM element that holds post's score value
     */
    private const SCORE_SELECTOR = 'span';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Main function. Starts crawl process, returns
     * scraped data to caller's class
     *
     * @return array
     */
    public function crawl(): array
    {
        $target_url = config('services.scraper.target_url');
        $crawler = $this->client->request('GET', $target_url);
        Log::info("Starting to scrape $target_url");

        $this->crawlRecursively($crawler);
        return $this->results;
    }

    /**
     * Recursion crawling function
     *
     * @param Crawler|null $crawler
     * @return void
     */
    private function crawlRecursively(?Crawler $crawler): void
    {
        if ($crawler) {
            // We need to slow down after each page switch, otherwise
            // target server will think we are DDOSing them.
            // This, of course, will slow down our crawling process in general
            // and could be optimized, using Guzzle's rate limiter package
            sleep(5);
            Log::debug("Scraping page $this->pageInProgress");

            $this->crawlPage($crawler);
        }
    }

    /**
     * Recursion's iteration function that calls
     * crawling itself
     *
     * @param Crawler $crawler
     * @return void
     */
    private function crawlPage(Crawler $crawler): void
    {
        $main_data = $this->getMainData($crawler);

        if ($main_data) {
            $additional_data = $this->getAdditionalData($crawler);

            $this->results[] = $this->buildResults($main_data, $additional_data);
            $nextPageLink = $crawler->filter('a.morelink');

            if ($nextPageLink->count() > 0) {
                $this->pageInProgress++;

                $this->client->setServerParameter('HTTP_USER_AGENT', CustomHelper::randomUserAgent());
                $nextPageCrawler = $this->client->click($nextPageLink->link());
                $this->crawlRecursively($nextPageCrawler);
            }
        }
    }

    /**
     * Gets main data, crawling DOM
     *
     * @param Crawler $crawler
     * @return array
     */
    private function getMainData(Crawler $crawler): array
    {
        if ($crawler->filter(self::DATA_START_SELECTOR)->count() > 0) {
            return $crawler->filter(self::DATA_START_SELECTOR)
                ->children(self::MAIN_DATA_DOM_SELECTOR)
                ->each(function (Crawler $main){
                    $url = $main->filter(self::URL_SELECTOR)->extract(['href']);
                    $title = $main->filter(self::TITLE_SELECTOR)->first()->innerText();
                    $id = $main->extract(['id']);

                    return [
                        'id' => (int)$id[0],
                        'title' => $title,
                        'url' => $url[0]
                    ];
                });
        }

        return [];
    }

    /**
     * Gets additional data, crawling DOM
     *
     * @param Crawler $crawler
     * @return array
     */
    private function getAdditionalData(Crawler $crawler): array
    {
        return $crawler->filter(self::ADDITIONAL_DATA_DOM_SELECTOR)->each(function (Crawler $additional){
                    $date_value = $additional->filter(self::DATE_SELECTOR)->first()->extract(['title']);
                    $score_value = $additional->filter(self::SCORE_SELECTOR)->first()->innerText();

                    return [
                        'date' => Carbon::parse($date_value[0]),
                        'score' => CustomHelper::explodeStringByWhitespaceAndToInt($score_value)
                    ];
                });
    }

    /**
     * Builds results for last return
     *
     * @param array $main_data
     * @param array $additional_data
     * @return array
     */
    private function buildResults(array $main_data, array $additional_data): array
    {
        $results = [];

        foreach ($main_data as $record_index => $record_data) {
            $results[$record_index] = [
                'id' => $record_data['id'],
                'title' => $record_data['title'],
                'url' => $record_data['url'],
                'date' => $additional_data[$record_index]['date'],
                'score' => $additional_data[$record_index]['score'],
            ];
        }

        return $results;
    }
}
