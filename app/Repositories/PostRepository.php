<?php

namespace App\Repositories;

use App\Interfaces\PostRepositoryInterface;
use App\Models\Post;
use Illuminate\Pagination\LengthAwarePaginator;

class PostRepository implements PostRepositoryInterface
{
    public function getAllByFilter(?string $title, ?string $orderBy, ?string $orderDirection): LengthAwarePaginator
    {
        $limit = config('services.scraper.records_per_page');

        return Post::where('title', 'like', "%$title%")
            ->orderBy($orderBy, $orderDirection)
            ->paginate($limit);
    }

    public function getByExternalId(int $externalId): ?Post
    {
        return Post::firstWhere('external_id', $externalId);
    }

    public function insert(array $data): Post
    {
        return Post::create($data);
    }
}
