<?php

namespace App\Interfaces;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Post;

interface PostRepositoryInterface
{
    public function getAllByFilter(?string $title, string $orderBy, string $orderDirection): LengthAwarePaginator;
    public function getByExternalId(int $externalId): ?Post;
    public function insert(array $data): Post;
}
