<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\PostService;

class GatherPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Post data
     *
     * @var array
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @var array
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @var PostService
     * @return void
     */
    public function handle(PostService $postService)
    {
        $postService->createOrUpdatePost($this->data);
    }
}
