FROM php:7.4-fpm

WORKDIR /var/www

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY --chown=www-data:www-data . /var/www

RUN composer install --no-interaction --optimize-autoloader --no-dev
RUN composer update

RUN cp .env.example .env

RUN php artisan key:generate

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash

RUN apt-get update \
  && apt-get install -y nodejs

RUN npm install
RUN npm run prod

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

EXPOSE 9000
CMD ["php-fpm"]
